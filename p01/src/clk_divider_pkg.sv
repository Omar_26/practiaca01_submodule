/********************************************************************************
* Module's Name: CLK_DIVIDER_PKG.sv
* Description: This is the package for data types in the clock divider
* Author(s): Sabina Bruce
* Date: September 6, 2020
********************************************************************************/

/*package to define the types of the signals that we are going to use in the clock divider module*/

`ifndef CLK_DIVIDER_PKG
	`define CLK_DIVIDER_PKG
package clk_divider_pkg;


typedef bit Clock_Signal_t;
typedef bit Reset_t;
typedef bit General_Bit_t;



endpackage

`endif