//Author: 		Sabina Bruce & Fer Munoz & Omar Anguiano
//Date:   		September 20, 2020
//Name: 		combinational_sequence.sv
//Description: Combination section combining modules 

//parametric module to define the inputs and outputs.
module combinational_sequence
import fsm_moore_pkg::*;
#(
parameter DW = 4,
parameter DBLW = 2*DW
)
(
input bit    clk,
input bit    rst, 
input logic  shift,

input logic [DW-1:0] mr,		//multiplier input
input logic [DW-1:0] md,		//multiplicand input
input bit initial_count,		//bit for the counter flag

output logic [DBLW-1:0] product,	//product of the operations 
output bit sign				//sign for the product
);


/*local variables for the module and its operations*/
bit xored;
/*size of mr and md*/
logic [DW-1:0] mux_concat_var, complement_mux_mr;
/*size of mr+md+1*/
logic [DBLW:0] mux_output_selected_md, sum_mux_output, concat_adder_full_length_one;
logic [DBLW:0] concat_mux_full_length_two, product_temp,mux_shift_full_length;


/*module parametric to make the two´s complement to the multiplier*/
twos_complement #(.DW(DW)) 
complement_mr
(
	.input_data(mr), 				//enters the multiplier
	.output_two_complement(complement_mux_mr) 	//outs the multiplier´s complement
);
	
	
/*This multiplexer decides whether a value will be added or subtracted*/
mux_module
 #(
.DW(DW)
)
add_subrtract_mux
(
    .select(product_temp[0]),     			// Selector last bit of product
    .datain(complement_mux_mr),    			// input of mr
    .datain2(mr), 					// complement 2s of mr
    .sltd_o(mux_concat_var)     			// Selected data
);


/*Concatanates input variable to produce full length working word*/
Concat #(.DW(DW))
add_subtract_concat
(
	.Data_MR(mux_concat_var), 			//Multiplier
	.Data_MD(md), 					//Multiplicand
	/**Size of output = 1+size(mr)+size(md)*/
	.Data_Concat_Add(concat_adder_full_length_one), //ouput of mr that goes to the adder
	.Data_Concat_Prod(concat_mux_full_length_two)	//output of md that goes to the multiplexer
	);	 

/*Adds the chosen mr value and md*/
Sum #(.DW(DBLW))
adder
(
	.Input_augend(concat_adder_full_length_one),
	.Input_addend(mux_output_selected_md),
	.Out_sum(sum_mux_output)
	
);	

/*combinational always to */
always_comb begin
/*Xnor component, used for the multiplexer selector */
xored = mux_output_selected_md[1]^mux_output_selected_md[0];
end

/*multiplexer parametric that decides to pass the value of the add or leave the same old one*/
mux_module
 #(
.DW(DBLW+1)
)
value_to_shift
(
    .select(xored),     					// Selector last bit of product
    .datain(mux_output_selected_md),     	// input of mr
    .datain2(sum_mux_output), 				//
    .sltd_o(mux_shift_full_length)      	// Selected data
);

/*this module conects the inputs and  outputs for the shift module*/
shift 
#(
.DW(DBLW)
)
shifting_right
(
	.clk(clk),				
	.shift(shift),
	.rst(rst),
	
	.datain(mux_shift_full_length),
	.out_shifted(product_temp)

);

/*mux of the multiplier, decides between the md original or the output from the shift */
mux_module
 #(
.DW(DBLW+1)
)
new_md_value
(
    .select(!initial_count),     				// Selector last bit of product
    .datain(concat_mux_full_length_two),     	// input of md
    .datain2(product_temp), 					// output from the shift
    .sltd_o(mux_output_selected_md)     		// Selected data
);

assign product = product_temp[DBLW:1];		// final result 
assign sign = product_temp[DBLW];		// sign for the result

endmodule