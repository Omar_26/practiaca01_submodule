//Author: 		Sabina Bruce
//Date:   		September 20, 2020
//Name: 			control.sv
//Description: control top file combining counter and state machine

/**TODO Remove PLL and clk divider
 * */

module control
import fsm_moore_pkg::*;
#(
parameter DW = 4,
parameter DBLW = 2*DW
)
(
input bit start,
input bit rst,
input bit clk,

output bit reduced_clk,
output bit start_count_shift,
output bit initial_cycle,
output bit ready
);

/*local variables*/
bit clk_1hz;
bit enable_start, start_count, counter_ovf;
logic [DW-1:0] count_fsm;
logic pll_out;	

/*this module receive the clock from the FPGA and gives a signal clock with different value*/
`ifdef SYNTHESIS
/*parametric module state machine for the multiplier*/
multiplier_fsm
#(
.DW(DW)
)
state_machine
(
.start(start),
.stop(counter_ovf),
.clk(clk),
.rst(rst),
.count(count_fsm),
.counter_enable(start_count),
.ready(ready),
.initial_cycle(initial_cycle)
);


bin_counter_ovf 
#(
.DW(DW), 
.MAXCNT(DW)
)
control_counter(
.clk(clk),
.rst(rst),
.enb(start_count),
.ovf(counter_ovf),
.count(count_fsm)
);

`else

/*PLL INSTANCE*/
/**NOTE: */
pll_50M_2_5M ins_pll
(
	.refclk(clk),   /**Reference Clock from FPGA*/
	.rst(~rst),      /**Reset button (Active high)*/
	.outclk_0(pll_out)  /**PLL output (2.5Mhz)*/
); 


top_clk_divider
#(
	.FREQUENCY(1), /**Desired*/
	.REFERENCE_CLOCK(2_500_500)/**PLL at 2.5Mhz*/
)
clk_divide
( 
 .Clk_FPGA(pll_out),		 // FPGA's clock as reference
 .reset(rst), 			 //reset signal to initiate new capture
 .Clock_Signal(clk_1hz) 	 //output signal created
);

dbcr_top
debounce_start
(
    // clk is the FPGA clock or the PLL output adjust the parameter type accordingly
    .clk(clk_1hz),
    // reset low active asynchronous 
    .rst_n(rst),
    // Input port 
    .Din(start),
    // One shot output port
    .one_shot(enable_start)
);

/*parametric module state machine for the multiplier*/
multiplier_fsm
#(
.DW(DW)
)
state_machine
(
.start(enable_start),
.stop(counter_ovf),
.clk(clk_1hz),
.rst(rst),
.count(count_fsm),
.counter_enable(start_count),
.ready(ready),
.initial_cycle(initial_cycle)
);


bin_counter_ovf 
#(
.DW(DW), 
.MAXCNT(DW)
)
control_counter(
.clk(clk_1hz),
.rst(rst),
.enb(start_count),
.ovf(counter_ovf),
.count(count_fsm)
);

`endif

/*parametric module state machine for the multiplier*/
/*multiplier_fsm
#(
.DW(DW)
)
state_machine
(
.start(start),
.stop(counter_ovf),
.clk(clk_1hz),
.rst(rst),
.count(count_fsm),
.counter_enable(start_count),
.ready(ready),
.initial_cycle(initial_cycle)
);


bin_counter_ovf 
#(
.DW(DW), 
.MAXCNT(DW)
)
control_counter(
.clk(clk_1hz),
.rst(rst),
.enb(start_count),
.ovf(counter_ovf),
.count(count_fsm)
);*/

assign reduced_clk = clk_1hz;
assign start_count_shift = start_count;

endmodule