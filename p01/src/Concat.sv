// Coder:           Omar Alejandro Anguiano N�jar
// Date:            18/09/2020
// Name:            Concat.sv
// Description:     This module concats the inputs with and output

/*Base module, define the 2 possible inputs or outputs for this module*/    
 module Concat #(parameter DW = 4)
(
	input logic [DW-1:0] Data_MR, 		/**Multiplier*/
	input logic [DW-1:0] Data_MD, 		/**Multiplicand*/
	/**Size of output = 1+size(mr)+size(md)*/
	output logic [2*DW:0] Data_Concat_Add, 	//output for the instance for the mr, goes to the adder module
	output logic [2*DW:0] Data_Concat_Prod	//output for the instance for the multiplicand
);	 

/*local variables with respective parameters*/
logic [DW:0] zeros_right = 'd0;			
logic [DW-1:0] zeros_left = 'd0;
logic [2*DW:0] tmp1;
logic [2*DW:0] tmp3;
	 
 always_comb begin 
	 /**{} This operator concatenates the data*/
	 tmp1 = {Data_MR,zeros_right}; 
	 tmp3 = {zeros_left,Data_MD,1'b0}; 
 end
 
 assign Data_Concat_Add = tmp1;
 assign Data_Concat_Prod = tmp3;
 
 endmodule
 