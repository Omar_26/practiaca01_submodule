// Coder:       Abisai Ramirez Perez
// Date:        January 28th, 2020
// Name:        bin_counter_ovf.sv
// Description: This is a binary counter with overflow

/*This module is parametric*/
module bin_counter_ovf #(
parameter 
DW      = 4, 			//DW is the parameter of the input bits
MAXCNT  = 5			//MAXCNT is the parameter for the counter
)(
input           clk,
input           rst,
input           enb,
output logic    ovf,
output [DW-1:0] count
);

typedef logic [DW-1:0] cnt_t;
typedef logic          ovf_t;

typedef struct {
cnt_t       count;
ovf_t       ovf;
} cntr_t;

cntr_t cntr;

always_ff@(posedge clk, negedge rst) begin: counter
    if (!rst)
        cntr.count    <=  '0;
    else if (enb)
        if (cntr.count >= MAXCNT-1'b1)
            cntr.count    <= '0;
        else
            cntr.count    <= cntr.count + 1'b1;
end:counter

always_comb begin: comparator
    cntr.ovf     =   (cntr.count >= (MAXCNT - 1'b1) ); 
end:comparator

assign count    =   cntr.count;
assign ovf      =   cntr.ovf;

endmodule