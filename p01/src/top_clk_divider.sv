/********************************************************************************
* Module's Name: top_clk_divider.sv
* Description: This is the top module that will be used to divide a clock signal
* Inputs: FPGA's clock signal used as a reference and the reset signal.
* Outputs: A clock signal which has a lower frequency than the reference input
* signal.
* Author: Sabina Bruce
* Date: September 6, 2020
********************************************************************************/

module top_clk_divider
import clk_divider_pkg::*;
	#(
		parameter FREQUENCY = 10000,
		parameter REFERENCE_CLOCK = 50000000,
		parameter  DW = $clog2(REFERENCE_CLOCK/(FREQUENCY*2))
		)
( 
 input  Clock_Signal_t  Clk_FPGA, 		// FPGA's clock as reference
 input  Reset_t         reset, 			//reset signal to initiate new capture
 output Clock_Signal_t  Clock_Signal 		//output signal created

);

logic [DW-1:0] count_r = 0;
logic [DW-1:0] max_count;
Clock_Signal_t temp_clk;

assign max_count = (REFERENCE_CLOCK/(FREQUENCY*2))-1'b1;

always_ff @(posedge Clk_FPGA,negedge reset) begin
	if( !reset )
	begin
		count_r  <= 0;
		temp_clk <= 0;
	end
	else
	begin
		if (count_r >= max_count)
		begin
			count_r <= 0;
			temp_clk <= ~temp_clk;
		end
		else
		begin
		count_r <= count_r + 1'b1;
		end
	end
end
	
assign Clock_Signal = temp_clk;
	
	

endmodule
