// Coder:           Omar Alejandro Anguiano N�jar
// Date:            19/09/2020
// Name:            twos_complement.sv
// Description:     This module outputs the two complement of the input

`ifndef TWOS_COMPLEMENT_SV
    `define TWOS_COMPLEMENT_SV
    
  module twos_complement #(parameter DW = 4)
(
	input logic [DW-1:0] input_data, 
	output logic [DW-1:0] output_two_complement 
	);
	  
logic [DW-1:0] temp;	/**Temporal variable*/
   
always_comb begin
	/**We make one complement then add 1 for twos complement*/
	temp = ~(input_data) + 1'b1; 
end	

assign output_two_complement = temp;

endmodule    
`endif     