// Coder:           Fernanda Munoz & Sabina Bruce & Omar Anguiano	
// Date:            18/09/2020
// Name:            Mux.sv
// Description:     



module mux_module
import mux_pkg::*;
 #(
//Parameters
    parameter 	DW = 4

)
	
(
    input logic  select,     // Selector
    input logic  [DW-1:0]   datain,     // Incoming data bus 
    input logic  [DW-1:0]   datain2,
    output logic [DW-1:0]   sltd_o      // Selected data
);


// Description of a multiplexer two to one using a case statement
always_comb begin
    case(select)
    0:          sltd_o = datain; 
	 1:  sltd_o = datain2;
   // default:    sltd_o = datain2[select];   //
    endcase
end


endmodule 
