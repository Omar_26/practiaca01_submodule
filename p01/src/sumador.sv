// Coder:           Fernanda Muï¿½oz Aguirre 	
// Date:            07/09/2020
// Name:            Sumador.sv
// Description:     This is adder module of our system

`ifndef Sum_SV
    `define Divider_SV


module Sum #(
//parameter
parameter 	DW = 4
)
(
	input logic  [DW:0] Input_augend,
	input logic  [DW:0] Input_addend,
	output logic [DW:0] Out_sum
	
);

//operation of the sumatory
always_comb begin: sum 
	
    Out_sum   = Input_augend +  Input_addend;

end: sum

	
endmodule 

`endif