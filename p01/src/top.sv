/********************************************************************************
* Module's Name: top.sv
* Description: This is the top module to englobe all the project
* Author: Fernanda Munoz & Sabina bruce
* Date: September 20, 2020
********************************************************************************/

/*this module is parametric, here we define the inputs and outputs of the top of our system*/
module top
	#(
	parameter DW = 4,
	parameter DBLW = 2*DW
)
( 
 input logic [DW-1:0] mr,		//input from the SW to the multiplier value
 input logic [DW-1:0] md,		//input from the SW to the multiplicand value
 input bit clk,				//input clk from FPGA
 input bit rst,				//input signal of reset
 input logic start,			//input signal, flag of start
 output logic [DBLW-1:0] product,	//general output, product value
 output logic sign,			//output product sign
 output logic ready			//output for the Led that marks the end of the cycles.
 
);

//wires
logic shift;
logic init;
bit clk_1hz;

//instance for the module of the combinational sequence, the one that makes every calculation of our system
`ifdef SYNTHESIS
combinational_sequence
#(
	.DW(DW)
)
comb_seq_module
 (
	.clk(clk),
	.rst(rst),
	.shift(shift),
	.mr(mr),
	.md(md),
	.initial_count(init),				//comes from control module
	.product(product),
	.sign(sign)

);

//instance for the control module
 control
 #(
	.DW(DW)
)
 control_module
 (
	.start(start),
	.rst(rst),
	.clk(clk),
	.reduced_clk(clk_1hz),
	.start_count_shift(shift),			
	.initial_cycle(init),				//goes to the initial count of the combinational sequence
	.ready(ready)
);

`else
combinational_sequence
#(
	.DW(DW)
)
comb_seq_module
 (
	.clk(clk_1hz),
	.rst(rst),
	.shift(shift),
	.mr(mr),
	.md(md),
	.initial_count(init),				//comes from control module
	.product(product),
	.sign(sign)

);

//instance for the control module
 control
 #(
	.DW(DW)
)
 control_module
 (
	.start(start),
	.rst(rst),
	.clk(clk),
	.reduced_clk(clk_1hz),
	.start_count_shift(shift),			
	.initial_cycle(init),				//goes to the initial count of the combinational sequence
	.ready(ready)
);
`endif


endmodule
