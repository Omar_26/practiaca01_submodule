//Author: 		Sabina Bruce
//Date:   		September 19, 2020
//Name: 			multiplier_fsm.sv
//Description: state machine in charge of sequence



module multiplier_fsm
import fsm_moore_pkg::*;

#(
parameter DW      = 4
)
(
input Bit_t start,
input Bit_t stop,
input Clock_Signal_t clk,
input Reset_t rst,
input logic [DW-1:0] count,
output Bit_t counter_enable,
output Bit_t ready,
output Bit_t initial_cycle
);



state_e current_state = STATE_IDLE;
Bit_t temp;


always_ff@(posedge clk, negedge rst)begin
if(!rst)begin
	current_state <= STATE_IDLE;
end
else begin
current_state<= STATE_IDLE;
	case(current_state)
		STATE_IDLE:begin
		if (start)
		current_state <= STATE_ZERO;
		else 
		current_state <= STATE_IDLE;
		end
		STATE_ZERO: begin
			if(stop)
			current_state <= STATE_IDLE;
			else
			current_state <= STATE_ZERO;
		end
		default: current_state <= STATE_IDLE;
endcase
end
end


always_comb begin
	case(current_state)
		STATE_IDLE: begin 
		ready = LED_ON;
		counter_enable = DISABLED;
		end
		STATE_ZERO: begin 
		counter_enable = ENABLED;
		ready = LED_OFF;
		end
		default:begin ready = LED_OFF;
		counter_enable = DISABLED;
		end
endcase
end

always_comb begin
if (count == 'd0)
 temp = 1'b1;
else
 temp = 1'b0;
end

assign initial_cycle = temp;

endmodule
