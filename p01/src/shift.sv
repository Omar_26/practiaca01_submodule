// Coder:           Fernanda Munoz & Sabina Bruce & Omar anguiano	
// Date:            07/09/2020
// Name:            shift.sv
// Description:    

`ifndef Shift_SV
    `define Shift_SV


module shift 
import fsm_moore_pkg::*;
#(
//parameter
parameter 	DW = 4

)
(
	input Clock_Signal_t clk,
	input Bit_t shift,
	input Reset_t rst,
	
	input logic  [DW:0]   datain,
	output logic [DW:0]  out_shifted

);
	
	
always_ff @(posedge clk, negedge rst) begin
	
if(!rst)
out_shifted <= {datain[DW],datain[DW:1]};	//we copy the most significative bit and concatenate it with the rest except for the lees significative bit
else begin
if (shift)
 out_shifted <= {datain[DW],datain[DW:1]};	//we copy the most significative bit and concatenate it with the rest except for the lees significative bit
else 
out_shifted <= out_shifted;//'d0;				// = out_shifted (possible solution)
end

end

endmodule 

`endif