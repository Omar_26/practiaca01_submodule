/********************************************************************************
* Module's Name: FSM_MOORE_PKG.sv
* Description: This is the package for data types in the state machine
* Author(s): Sabina Bruce
* Date: September 13, 2020
********************************************************************************/


`ifndef FSM_MOORE_PKG
	`define FSM_MOORE_PKG
package fsm_moore_pkg;


typedef bit Clock_Signal_t;
typedef bit Reset_t;
typedef bit Bit_t;

typedef enum logic{
	LED_ON = 1'b1,
	LED_OFF = 1'b0
}led_e;

typedef enum logic{
	ENABLED = 1'b1,
	DISABLED = 1'b0
} en_e;
	
typedef enum logic [3:0] {
	STATE_ZERO  = 4'b0000,
	STATE_ONE   = 4'b0001,
	STATE_TWO   = 4'b0010,
	STATE_THREE = 4'b0011,
	STATE_FOUR  = 4'b0100,
	STATE_FIVE  = 4'b0101, 
	STATE_SIX   = 4'b0110,
	STATE_SEVEN = 4'b0111,
	STATE_EIGHT = 4'b1000,
	STATE_NINE  = 4'b1001,
	STATE_IDLE  = 4'b1010
}state_e;


endpackage

`endif