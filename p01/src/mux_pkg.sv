// Coder:           DSc Abisai Ramirez Perez
// Date:            24 April 2019
// Name:            mux_pkg.sv
// Description:     This is the package of the parametric multiplexer.

`ifndef MUX_PKG_SV
    `define MUX_PKG_SV
package mux_pkg;

localparam  W_DW    = 2;    // Data width of each input/output
localparam  N_SEL   = 2;    // Selection bits
localparam  N_8SEL  = 3;    // Selection bits
localparam  ONE     = 1;    // parameter
localparam  TWO     = 2;    // parameter
localparam  THREE   = 3;    // parameter
typedef logic [W_DW-1:0]                    dtwidth_t;  // Input/ output data width
typedef logic [N_SEL-1:0]                   selectr_t;  // Selector data type  
typedef logic [N_8SEL-1:0]                  sel_in_t;  // Selector data type  


//TODO: This is a multidimensional array (MDA), Which is the correct selection/addressing order?
//TODO: Is this a packed or unpacked array?

typedef enum logic [1:0] {
    FETCH = 2'd0,
    DECOD = 2'd1,
    EXECT = 2'd2,
    STORE = 2'd3
} mux_op_code_t;

typedef logic [W_DW-1:0]                    in_bus_t [2**N_SEL-1:0];   // Incoming bus data to multiplexer
typedef logic [W_DW-1:0]                    in_dt_8_t [2**N_8SEL-1:0];   // Incoming bus data to multiplexer
typedef mux_op_code_t                       in_bus_enum_t [2**N_SEL-1:0];   // Incoming bus data to multiplexer


endpackage
`endif
