`timescale 1ns / 1ps
module tb_control;

logic start;
logic rst;
logic clk;
logic ready;

control
uut
(
.start(start),
.rst(rst),
.clk(clk),
.ready(ready)
);

initial begin
        clk     = 0;
	rst = 0;
	start = 0;
#2000 rst = 1;
#1000 start = 1;
#100000 start = 0;
    $stop;
end

always begin
    #1 clk <= ~clk;
end

endmodule
