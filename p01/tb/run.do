if [file exists work] {vdel -all}
vlib work
vlog +define+SYNTHESIS -f files.f
onbreak {resume}
set NoQuitOnFinish 1
vsim -voptargs=+acc work.tb_seq_mult
do wave.do
run 1300ms
