`timescale 1ns / 1ps
module tb_top;

localparam DW = 4;
localparam PERIOD = 2;
 logic [DW-1:0] mr;
 logic [DW-1:0] md;
 logic clk;
 logic rst;
 logic start;
 logic [2*DW-1:0] product;
 logic sign;
 logic ready;

top
uut
( 
 .mr(mr),
 .md(md),
 .clk(clk),
 .rst(rst),
 .start(start),
 .product(product),
 .sign(sign),
 .ready(ready)
 
);

initial begin
	
        clk     = 0;
	rst     = 0;
   #200 rst     = 1;
	mr = -(2**(DW-1));
	md = -(2**(DW-1));
	
repeat(256) begin
	     start   = 0;
  	     start   = 1;
   #(PERIOD) start   = 0;
@(posedge ready);
#1{mr, md} = {mr, md}+1;
end
#100
    $stop;
end

always begin
    #1 clk <= ~clk;
end

endmodule
