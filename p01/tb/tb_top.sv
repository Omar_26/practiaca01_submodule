`timescale 1ns / 1ps
module tb_top;

localparam DW = 4;

 logic [DW-1:0] mr;
 logic [DW-1:0] md;
 logic clk;
 logic rst;
 logic start;
 logic [2*DW-1:0] product;
 logic sign;
 logic ready;

top
uut
( 
 .mr(mr),
 .md(md),
 .clk(clk),
 .rst(rst),
 .start(start),
 .product(product),
 .sign(sign),
 .ready(ready)
 
);

initial begin
	
    clk     = 0;
	rst     = 0;
	mr = -(2**(DW-1));
	md = -(2**(DW-1));
	start   = 0;
   #10 rst     = 1;
	start   = 1;
repeat(2**DW) begin
	   start   = 1;
    #4 start   = 0;
	@(posedge ready);
	   mr = mr + 1'b1;
	   md = md + 1'b1;
end
    $stop;
end

always begin
    #1 clk <= ~clk;
end

endmodule
