onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_seq_mult/clk
add wave -noupdate /tb_seq_mult/start
add wave -noupdate -radix symbolic /tb_seq_mult/multiplier
add wave -noupdate -radix symbolic /tb_seq_mult/multiplicand
add wave -noupdate -radix symbolic /tb_seq_mult/product
add wave -noupdate /tb_seq_mult/ready
add wave -noupdate /tb_seq_mult/uut/comb_seq_module/mr
add wave -noupdate /tb_seq_mult/uut/comb_seq_module/md
add wave -noupdate /tb_seq_mult/uut/comb_seq_module/xored
add wave -noupdate /tb_seq_mult/uut/comb_seq_module/mux_concat_var
add wave -noupdate /tb_seq_mult/uut/comb_seq_module/complement_mux_mr
add wave -noupdate /tb_seq_mult/uut/comb_seq_module/mux_output_selected_md
add wave -noupdate /tb_seq_mult/uut/comb_seq_module/sum_mux_output
add wave -noupdate /tb_seq_mult/uut/comb_seq_module/concat_adder_full_length_one
add wave -noupdate /tb_seq_mult/uut/comb_seq_module/concat_mux_full_length_two
add wave -noupdate /tb_seq_mult/uut/comb_seq_module/product_temp
add wave -noupdate /tb_seq_mult/uut/comb_seq_module/mux_shift_full_length
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 2} {259938 ps} 0} {{Cursor 2} {43723588608 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 335
configure wave -valuecolwidth 71
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {251147 ps} {293361 ps}
