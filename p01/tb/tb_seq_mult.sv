`timescale 1ns / 1ps

module tb_seq_mult;

//FIXME[DEV]: here import from your package
//import seq_mul_pkg::*;
//localparam IN_DW =  seq_mul_pkg::DW;
localparam IN_DW = 4;

//Inputs
logic			         start = 0;
logic			         clk = 1;
logic				      rst = 0;
logic [IN_DW-1:0]	   multiplier;
logic [IN_DW-1:0]	   multiplicand ;
logic [2*IN_DW-1:0]  product;
logic				      ready;
//data_segments_t      o_segments_units;
//data_segments_t      o_segments_tens;
//data_segments_t      o_segments_hundreds;
logic sign;

/*
HERE AN INSTANCE OF 
your sequential multiplier.
*/
top	
uut
( 
 .mr(multiplier),
 .md(multiplicand),
 .clk(clk),
 .rst(rst),
 .start(start),
 .product(product),
 .sign(sign),
 .ready(ready)
 
);

// DO NOT TOUCH //
// The code from here to the end //
always #1 clk <= ~clk;
integer cont;
integer m0_temp;
integer m1_temp;
initial begin
   rst = 0;
   #2 rst = 1;
   start = 0;
   clk = 0;
   multiplier   = -(2**(IN_DW-1));
   multiplicand = -(2**(IN_DW-1));
   repeat (2**IN_DW) begin
      $display("------Tabla del %d------",$signed(multiplier));
      repeat (2**IN_DW) begin
         m0_temp = multiplier    ;
         m1_temp = multiplicand    ;
         start = 1;  #2
         start = 0;
         #(2*IN_DW+5)     // Waiting N+2.5 cycles 
         $display("%d * %d = %d",$signed(multiplier),$signed(multiplicand),$signed(product));
         if (($signed(multiplier)*$signed(multiplicand) != $signed(product)) ) begin
            cont = cont + 1 ;
            $display("[%0dns] MISMATCH AT %d * %d = %d | FOUND = %d",$time, $signed(multiplier),$signed(multiplicand),$signed(multiplicand*multiplier),$signed(product));
         end
         multiplicand = multiplicand+1;
        end
        multiplier = multiplier+1;
    end   
    $display("%d",cont);
    #500
    $stop;
end
 


endmodule
