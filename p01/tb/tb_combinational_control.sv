`timescale 1ns / 1ps
module tb_combinational_control;

logic shift;
logic rst;
logic clk;
logic [3:0] mr;
logic [3:0] md;
logic initial_count;
logic [7:0] product;
logic sign;


combinational_sequence
#(
.DW(4)
)
uut
(
.clk(clk), 
.shift(shift),
.rst(rst),


.mr(mr),
.md(md),
.initial_count(initial_count),

.product(product),
.sign(sign)

);

initial begin
	mr = 4'b0100;
	md = 4'b0011;
        clk     = 0;
	rst     = 0;
	shift   = 0;
	initial_count = 0;
	#2000 rst = 1;
	#20 shift =1;
	 initial_count = 1;
	#1 initial_count = 0;

    $stop;
end

always begin
    #1 clk <= ~clk;
end

endmodule
